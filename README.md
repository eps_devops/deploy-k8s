# Déploiement de l'application `rancher-demo` avec Kubernetes

Ce guide permet de déployer l'application `rancher-demo` sur un cluster Kubernetes. Il est compatible avec **Minikube** et **Docker Desktop**.

---

## Prérequis

- Kubernetes activé sur :
  - **Minikube** ([guide d'installation](https://minikube.sigs.k8s.io/docs/start/))
  - **Docker Desktop** ([guide d'installation](https://www.docker.com/products/docker-desktop/))
- Outils nécessaires :
  - `kubectl` installé
  - Lens (facultatif, pour visualiser vos clusters)

---

## Configurer le contexte Kubernetes

### Pour Minikube

```bash
kubectl config use-context minikube
minikube start
```

### Pour Docker Desktop

```bash
kubectl config use-context docker-desktop
```

---

## Étape 1 : Créer et configurer un Pod

1. Créez un fichier `rancher-demo-pod.yaml` et ajoutez le contenu suivant :

    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: rancher-demo-pod
    spec:
      containers:
      - name: rancher-demo
        image: salahgo/rancher-demo
        ports:
        - containerPort: 8080
    ```

2. Appliquez la configuration :

    ```bash
    kubectl apply -f rancher-demo-pod.yaml
    ```

3. Vérifiez que le Pod est créé :

    ```bash
    kubectl get pods
    ```

---

## Étape 2 : Passer à un Déploiement

Un **Deployment** permet de gérer les Pods de façon plus robuste.

1. Créez un fichier `rancher-demo-deploy.yaml` avec le contenu suivant :

    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: rancher-demo
      labels:
        app: rancher-demo
    spec:
      replicas: 2
      selector:
        matchLabels:
          app: rancher-demo
      template:
        metadata:
          labels:
            app: rancher-demo
        spec:
          containers:
          - name: rancher-demo
            image: salahgo/rancher-demo
            ports:
            - containerPort: 8080
    ```

2. Appliquez le fichier de déploiement :

    ```bash
    kubectl apply -f rancher-demo-deploy.yaml
    ```

3. Vérifiez le déploiement et les Pods créés :

    ```bash
    kubectl get deployments
    kubectl get pods
    ```

4. Modifiez le nombre de réplicas en modifiant la ligne `replicas: 2` et réappliquez :

    ```bash
    kubectl apply -f rancher-demo-deploy.yaml
    ```

---

## Étape 3 : Exposer l'application avec un Service NodePort

1. Créez un fichier `rancher-demo-svc.yaml` avec le contenu suivant :

    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: rancher-demo-service
      labels:
        app: rancher-demo
    spec:
      type: NodePort
      selector:
        app: rancher-demo
      ports:
      - port: 8080
        targetPort: 8080
    ```

2. Appliquez le fichier :

    ```bash
    kubectl apply -f rancher-demo-svc.yaml
    ```

3. Vérifiez le service créé :

    ```bash
    kubectl get services
    ```

### Accéder à l'application

#### Avec Minikube

```bash
minikube service rancher-demo-service
```

#### Avec Docker Desktop

1. Obtenez le **NodePort** :

    ```bash
    kubectl get service rancher-demo-service
    ```

2. Accédez à l'application à l'URL suivante :  
   `http://localhost:<NodePort>`

---

## Étape 4 : Ajouter un Service LoadBalancer (Optionnel)

1. Modifiez `rancher-demo-svc.yaml` pour remplacer `NodePort` par `LoadBalancer` :

    ```yaml
    spec:
      type: LoadBalancer
    ```

2. Appliquez les modifications :

    ```bash
    kubectl apply -f rancher-demo-svc.yaml
    ```

3. Vérifiez l'adresse IP externe attribuée :

    ```bash
    kubectl get service rancher-demo-service
    ```

4. Accédez à l'application via l'adresse IP affichée.

